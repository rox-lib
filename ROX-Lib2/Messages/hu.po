# Hungarian messages for ROX-Lib2.
# Copyright (C) 2005 Free Software Foundation, Inc.
# Andras Mohari <mayday@mailpont.hu>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: ROX-Lib2 2.0.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-05-27 11:47+BST\n"
"PO-Revision-Date: 2006-03-12 17:30+0100\n"
"Last-Translator: Andras Mohari <mayday@mailpont.hu>\n"
"Language-Team: Hungarian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: pygettext.py 1.5\n"
"X-Poedit-Language: Hungarian\n"

#: ../../AppRun:12
msgid ""
"ROX-Lib2 contains code that is useful to newer ROX applications. You should "
"place this directory somewhere where other programs can find it, such as ~/"
"lib or /usr/local/lib.\n"
"\n"
"ROX-Lib version: %s\n"
"PyGTK version: %s\n"
"GTK version: %s\n"
"\n"
"PyGTK location:\n"
"%s"
msgstr ""
"A ROX-Lib2 az újabb ROX alkalmazások számára hasznos kódot tartalmaz. Olyan "
"helyre másold ezt a könyvtárat, ahol a programok meg fogják találni. "
"Például: ~/lib vagy /usr/local/lib.\n"
"\n"
"ROX-Lib verzió: %s\n"
"PyGTK verzió: %s\n"
"GTK verzió: %s\n"
"\n"
"PyGTK helye:\n"
"%s"

#: OptionsBox.py:124
msgid "%s options"
msgstr "%s beállításai"

#: OptionsBox.py:127
msgid "_Revert"
msgstr "_Visszavonás"

#: OptionsBox.py:129
msgid "Restore all options to how they were when the window was opened"
msgstr "Visszaállítja az ablak megnyitásakor érvényes beállításokat"

#: __init__.py:72
msgid ""
"The pygtk2 package (2.0.0 or later) must be installed to use this program:\n"
"http://rox.sourceforge.net/desktop/ROX-Lib\n"
msgstr ""
"Ehhez a programhoz telepíteni kell a pygtk2 csomagot (2.0.0-t vagy "
"újabbat):\n"
"http://rox.sourceforge.net/desktop/ROX-Lib\n"

#: __init__.py:80
msgid "Broken pygtk installation: found pygtk (%s), but not gtk!\n"
msgstr "Hibás a pygtk telepítése: van pygtk (%s), de nincs gtk!\n"

#: __init__.py:96
msgid ""
"WARNING from ROX-Lib: the version of findrox.py used by this application (%"
"s) is very old and may cause problems."
msgstr ""

#: __init__.py:117
msgid "Operation aborted at user's request"
msgstr "Művelet megszakítva a felhasználó kérésére"

#: __init__.py:124 debug.py:44
msgid "Error"
msgstr "Hiba"

#: __init__.py:150
msgid "Information"
msgstr "Információ"

#: __init__.py:172
msgid "Confirm:"
msgstr "Megerősítés:"

#: __init__.py:454
msgid ""
"You do not have the Python 'xml' module installed, which ROX-Lib2 requires. "
"You need to install python-xmlbase (this is a small package; the full PyXML "
"package is not required)."
msgstr ""
"Nincs telepítve a ROX-Lib2 működéséhez szükséges \"xml\" Python modul. "
"Telepíteni kell a python-xmlbase csomagot (ez csak egy kis csomag; a teljes "
"PyXML csomagra nincs szükség)."

#: debug.py:34
msgid "_Details"
msgstr "_Részletek"

#: debug.py:89
msgid "_Bug Report"
msgstr "_Hibajelentés"

#: debug.py:95
msgid "Forced Quit"
msgstr "Erőltetett kilépés"

#: debug.py:120
msgid "Stack trace (innermost last)"
msgstr "Veremlista (a legbelső van a végén)"

#: debug.py:215
msgid "Local variables in selected frame:"
msgstr "Helyi változók a kijelölt keretben:"

#: fileutils.py:22
msgid "Retry"
msgstr "Ismét"

# XXX: Megmutatja a fájlt egy ROX-Filer könyvtárablakban.
#: fileutils.py:27
msgid "Examine"
msgstr "Megjelenítés"

#: fileutils.py:33
msgid "Error:"
msgstr "Hiba:"

#: fileutils.py:59
msgid ""
"Could not create directory `%s' because a file already exists at that path.\n"
msgstr ""
"Nem sikerült létrehozni a könyvtárat (%s), mert már létezik ilyen nevű "
"fájl.\n"

#: fileutils.py:65
msgid "Could not create directory `%s'.\n"
msgstr "Nem sikerült létrehozni a könyvtárat: \"%s\".\n"

#: launch.py:11
msgid ""
"The program '%s' cannot be run, as the 0launch command is not available. It "
"can be downloaded from here:\n"
"\n"
"http://0install.net/injector.html"
msgstr ""
"Nem lehet futtatni a programot (%s), mert a 0launch parancs nem található. "
"Innen tölthető le:\n"
"\n"
"http://0install.net/injector.html"

#: loading.py:28
msgid ""
"Cannot load files from a remote machine (multiple files, or target "
"application/octet-stream not provided)"
msgstr ""
"Nem tudom betölteni másik gépről a fájlokat (vagy több fájl van, vagy nincs "
"megadva az application/octet-stream)"

#: mime.py:492
msgid ""
"The '%s' command returned an error code!\n"
"Make sure you have the freedesktop.org shared MIME package:\n"
"http://www.freedesktop.org/standards/shared-mime-info.html"
msgstr ""
"A parancs (%s) hibát jelzett!\n"
"Telepítve kell hogy legyen a freedesktop.org \"shared MIME\" csomagja:\n"
"http://www.freedesktop.org/standards/shared-mime-info.html"

#: mime_handler.py:52
msgid "Install %s"
msgstr "Telepítés: %s"

#: mime_handler.py:85
msgid "Type"
msgstr "Típus"

#: mime_handler.py:90
msgid "Name"
msgstr "Név"

#: mime_handler.py:96
msgid "Current"
msgstr "Jelenlegi"

#: mime_handler.py:103
msgid "Install?"
msgstr "Telepítés?"

#: mime_handler.py:109
msgid "Uninstall?"
msgstr "Eltávolítás?"

#: mime_handler.py:293
msgid ""
"Run actions can be changed by selecting a file of the appropriate type in "
"the Filer and selecting the menu option 'Set Run Action...'"
msgstr ""
"A programok hozzárendelését a fájlkezelőben lehet megváltoztatni. Jelölj ki "
"egy megfelelő típusú fájlt, majd használd a \"Program hozzárendelése...\" "
"menüpontot."

#: mime_handler.py:301
msgid "run action"
msgstr "program-hozzárendelés"

#: mime_handler.py:312
msgid "thumbnail handler"
msgstr "bélyegképkezelő"

#: mime_handler.py:313
msgid ""
"Thumbnail handlers provide support for creating thumbnail images of types of "
"file.  The filer can generate thumbnails for most types of image (JPEG, PNG, "
"etc.) but relies on helper applications for the others."
msgstr ""
"A bélyegképkezelők különféle típusú fájlokhoz hoznak létre bélyegképeket. A "
"fájlkezelő a gyakoribb képtípusokhoz (JPEG, PNG stb.) tud bélyegképeket "
"készíteni, más típusokhoz azonban segédprogramokat használ."

#: mime_handler.py:330
msgid "type handler"
msgstr "típuskezelő"

# XXX
#: mime_handler.py:331
msgid ""
"The application can handle files of these types.  Click on OK to add it to "
"the SendTo menu for the type of file, and also the customized File menu."
msgstr ""
"Ezeket a fájltípusokat képes kezelni az alkalmazás. Az OK gombra kattintva "
"lehet felvenni az alkalmazást a típushoz tartozó Küldés menübe, valamint a "
"Fájl menübe."

#: saving.py:169
msgid "Delete temporary file '%s'?"
msgstr "Törlöd az átmeneti fájlt (%s)?"

#: saving.py:345
msgid ""
"Drag the icon to a directory viewer\n"
"(or enter a full pathname)"
msgstr ""
"Dobd az ikont egy könyvtárablakba\n"
"(vagy adj meg egy teljes útvonalat)"

#: saving.py:371
msgid "Unnamed"
msgstr "Névtelen"

#: saving.py:453
msgid "'%s' already exists as a directory."
msgstr "\"%s\" egy már létező könyvtár."

#: saving.py:462
msgid "Permissions changed from %o to %o."
msgstr "Jogok frissítve. Régi: %o, új: %o."

#: saving.py:465
msgid "Size was %d bytes; now %d bytes."
msgstr "A méret %d byte volt; most %d byte."

#: saving.py:468
msgid "Modification time changed."
msgstr "Megváltozott a módosítási idő."

#: saving.py:474
msgid "File '%s' already exists -- overwrite it?"
msgstr "\"%s\" már létezik. Felülírod?"

#: saving.py:475
msgid "_Overwrite"
msgstr "_Felülírás"

#: saving.py:515
msgid "_Discard"
msgstr "El_vetés"

#: saving.py:525
msgid "Save As:"
msgstr "Mentés másként:"

#: thumbnail.py:175
msgid "Thumbnail not implemented"
msgstr ""
